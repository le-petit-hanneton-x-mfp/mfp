---
author: Matthieu Joly
title: About Us
date: 2023-05-02
description:
keywords: ["about-us", "about-mfp", "contact"]
type: about
---

Welcome to MFP Brasserie, a craft brewery with a passion for the art of beer and conviviality. We are a team of passionate brewers who believe that beer is more than just a drink - it is a symbol of conviviality, sharing and indulgence.

We pride ourselves on producing unique and tasty beers, using quality ingredients and respecting the environment. We are constantly looking for new flavors and new techniques to create beers that surprise and delight the taste buds.

We are passionate people led by the initiative of Matthieu, a passionate brewer who decided to create his own brewery to share his passion with others. Our name, MFP Brewery, is a tribute to our founder and his commitment to quality and innovation.

At MFP Brewery, we believe that beer is a beverage that should be shared. That's why we like to organize friendly events and beer tastings, so that everyone can discover and appreciate our creations. We also believe in curiosity and open-mindedness, and we like to explore new horizons to find ideas and inspiration.

We hope you will share our passion for beer and join us in celebrating conviviality and good food. Thank you for visiting our site and discovering our craft beers
