---
author: "Matthieu Joly"
title: "My little autumn"
date: 2023-05-20
description: "Discover my little autumn"
thumbnail: /images/beer/mon-petit-automne/paysages-automne.jpg
credits: "<a href=\"https://fr.freepik.com/photos-gratuite/paysages-automne_1254316.htm#query=automne&position=1&from_view=search&track=sph\">Image from photoangel</a> on Freepik"
translationKey: "my-little-autumn"
categories: ["season"]
---

<h2>Saison</h2>
<h4>American Amber Ale</h4>

A new season of new flavors. Although the temperature is dropping and the weather is getting worse, it's time to resume game nights by the fire with this seasonal beer.

**ABV**		4,7%<br>
**SRM**		19<br>
**IBU**		34,3<br>
