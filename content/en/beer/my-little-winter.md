---
author: "Matthieu Joly"
title: "My little winter"
date: 2023-05-19
description: "Discover my little winter"
thumbnail: /images/beer/mon-petit-hiver/4582497.jpg
credits: "<a href=\"https://fr.freepik.com/vecteurs-libre/paysage-hiver-au-design-plat_10832951.htm#query=hivers&position=8&from_view=search&track=sph\">Designed by Freepik</a>"
translationKey: "my-little-winter"
categories: ["season"]
---

<h2>Saison</h2>
<h4>Winter Seasonal Beer</h4>

The night is back and the cold is invading our hemisphere. It's time to warm up and share a new seasonal beer.

**ABV**		6,7%<br>
**SRM**		25<br>
**IBU**		26,9<br>
