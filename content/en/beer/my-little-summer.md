---
author: "Matthieu Joly"
title: "My little summer"
date: 2023-05-20
description: "Discover my little summer"
thumbnail: /images/beer/mon-petit-ete/8803353.jpg
credits: "<a href=\"https://fr.freepik.com/vecteurs-libre/fond-plat-pour-ete_40322931.htm#query=%C3%A9t%C3%A9&position=10&from_view=search&track=sph\">Designed by Freepik</a>"
translationKey: "my-little-summer"
categories: ["season"]
---

<h2>Saison</h2>
<h4>American IPA</h4>

Here comes the summer and its aperitifs on terraces, its barbecues, its hikes in the forest and its evenings at the campsite. Yes, but not without my beer, this seasonal beer is to be shared, like all those little precious moments with family and friends.

**ABV**		6,6%<br>
**SRM**		10<br>
**IBU**		52,1<br>