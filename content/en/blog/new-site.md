---
author: "Matthieu Joly"
title: "New site"
date: 2023-05-08
description: "Review of the new site, here a brief overview"
credits: "<a href=\"http://www.freepik.com\">Designed by stories / Freepik</a>"
tags: ["Info"]
thumbnail: /images/blog/nouveau-site/construct-website.jpg
translationKey: "new-site"
---

The new site is here. We have chosen to make a site in French and English. Indeed, more and more non-French speaking people are interested in MFP news and want to learn more about the brewery. This site has been entirely realized with the Hugo framework and the blist theme.

The site is divided into several sections. First, there are the beers, you will find all the beers produced or in production and their characteristics. Then there is the blog part, here we will publish posts about the brewery or more general on the manufacturing process or even on an element that we want to share. We will put tags on these posts to be able to find them more easily on the same theme. The search option is still available if you are looking for something specific.

This site supports dark mode so feel free to switch at your convenience. Best post will contain the post we want to highlight, it may change over time. You will also find our networks at the bottom of the page.

We want to bring more visibility on our project through this site. We will be able to keep you informed of the next events or news as they happen.

<h3>Then stayed tuned</h3>