---
author: "Matthieu Joly"
title: "Brewers"
date: 2023-05-23
description: "Discover the brewers"
tags: ["Brewery"]
thumbnail: /images/blog/les-brasseurs/Yannis-vs-Matthieu.png
translationKey: "brewers"
---

We are two friends with a passion for craft beers. We met in 2011 while studying computer science. After going through the bullshit process, it was in 2017 that we brewed our first beer together. From the start, we made the decision not to brew recipes made by others. We took the time to do the research necessary to create different recipes. We are testing them over time and until now we have had pretty good feedback.

# Matthieu

### Head brewer

{{< figure src="/images/blog/les-brasseurs/matthieu-portrait.jpg" alt="Matthieu's portrait" >}}

I am the main author of the recipes you will find on this site. Since 2017, I have been researching, experimenting and brewing to improve myself and discover new flavors. I started brewing all grain since my first batch on a 6 liter system. You don't need much to get started, a large pot, a strainer and a fermenter. My favorite beer style is still russian imperial stout, but I drink almost everything. I like to share my knowledge and imagine new recipes, classic or unusual.

# Yannis

### Brewer

{{< figure src="/images/blog/les-brasseurs/yannis-portrait.jpg" alt="Yannis's portrait" >}}

My best assistant, author of his first recipe, he attends almost every brew. He helps promote the brewery and has been pushing the thinking since 2017. He thinks big, he started with my help directly in all grain on a 30 liter system. For the past few years he has been interested in Sour. He has been harvesting as much wild yeast as possible to brew the best Sour. He appreciates when there is nothing left to do and he likes things done right.
