---
author: "Matthieu Joly"
title: "Cheh !"
date: 2023-06-13
description: "Présentation de la Cheh !"
categories: ["Autre"]
thumbnail: /images/beer/autre/43-chech.jpg
tags: ["Bière", "Brune", "Autre"]
---

<h2>Autre</h2>
<h4>Rauchbier</h4>

Pourquoi une bière fumée devrait avoir un goût de vieille saucisse ? On entre ici dans un subtil mélange entre la gastronomie et l’art brassicole. Un équilibre différent de ce que l’on retrouve dans le genre.


**ABV**		5% <br>
**SRM**		8 <br>
**IBU**		17,6 <br>