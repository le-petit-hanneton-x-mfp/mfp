---
author: "Matthieu Joly"
title: "Canberra"
date: 2023-06-03
description: "Présentation de la Canberra"
categories: ["World wide IPA"]
thumbnail: /images/beer/wwipa/33-canberra.jpg
credits: "Image de <a href=\"https://fr.freepik.com/vecteurs-libre/decouverte-australie_3835539.htm#query=australie&position=20&from_view=search&track=sph\">Freepik</a>"
tags: ["Bière", "IPA", "World wide IPA"]
---

<h2>World wide IPA</h2>
<h4>Rye IPA</h4>

Surfez sur la vague australienne et laissez ses saveurs exotiques vous emporter. Installez-vous confortablement au bord du lac Burley Griffin avec un panier de fruits étonnants. Regardez l’horizon et détendez-vous…

**ABV**		5% <br>
**SRM**		6 <br>
**IBU**		30,4 <br>