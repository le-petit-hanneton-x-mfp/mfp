---
author: "Matthieu Joly"
title: "Mon petit hiver"
date: 2023-05-24
description: "Présentation de mon petit hiver"
categories: ["Saison"]
thumbnail: /images/beer/saison/24-mon-petit-hiver.jpg
credits: "Image de <a href=\"https://fr.freepik.com/vecteurs-libre/paysage-hiver-au-design-plat_10832951.htm#query=hivers&position=8&from_view=search&track=sph\">Freepik</a>"
translationKey: "my-little-winter"
tags: ["Bière", "Winter", "Brune", "Saison"]
---

<h2>Saison</h2>
<h4>Winter Seasonal Beer</h4>

La nuit reprend le dessus et le froid envahit notre hémisphère. Il est temps de se réchauffer et de partager une nouvelle bière de saison. 

**ABV**		6,7% <br>
**SRM**		25 <br>
**IBU**		26,9 <br>