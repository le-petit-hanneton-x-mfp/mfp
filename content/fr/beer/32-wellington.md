---
author: "Matthieu Joly"
title: "Wellington"
date: 2023-06-02
description: "Présentation de la Wellington"
categories: ["World wide IPA"]
thumbnail: /images/beer/wwipa/32-wellington.jpg
credits: "<a href=\"https://fr.freepik.com/photos-gratuite/gros-plan-pic-isthme-lac-nouvelle-zelande_17245786.htm#query=nouvelle%20z%C3%A0lande&position=12&from_view=search&track=ais\">Image de wirestock</a> sur Freepik"
tags: ["Bière", "IPA", "World wide IPA"]
---

<h2>World wide IPA</h2>
<h4>American IPA</h4>

Bienvenue au pays des kiwis, marchons le long de Cuba street et admirons les couleurs et le soleil. La rue animée hume des parfums de pêche. Peut-être cela provient de ce marchand de glace ou peut-être de ce restaurant. Continuons jusqu’au port pour en être sûr.

**ABV**		5,8% <br>
**SRM**		7 <br>
**IBU**		44,3 <br>