---
author: "Matthieu Joly"
title: "Mirkobeer"
date: 2023-10-01
description: "Présentation de la Mirkobeer"
categories: ["Friends"]
thumbnail: /images/beer/friend/01-mirkobeer.jpg
tags: ["Bière", "Brune", "Friends"]
---

<h2>Friends</h2>
<h4>Belgian Dubbel</h4>

La Mirkobeer, une bière qui se déguste avec le sourire. Cette double vient vous câliner jusqu'à ce que vous tombiez dans les bras de Morphée. Attention vous risquez de rêver à créer une multinationales dans le secteur informatique mais ce n'est qu'un rêve. 

**ABV**		7,1% <br>
**SRM**		19 <br>
**IBU**		15,3 <br>