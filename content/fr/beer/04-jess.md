---
author: "Matthieu Joly"
title: "Jess"
date: 2023-10-04
description: "Présentation de la Jess"
categories: ["Friends"]
thumbnail: /images/beer/friend/04-jess.jpg
tags: ["Bière", "Blonde", "Friends"]
---

<h2>Friends</h2>
<h4>Gluten free</h4>

Rayonnante comme le soleil, cette bière sans gluten vous emmènera sur des terres inconnues. Son parfum envoûtant et son amertume ne vous laisseront pas de marbre.

**ABV**		6,5% <br>
**SRM**		3 <br>
**IBU**		22,9 <br>