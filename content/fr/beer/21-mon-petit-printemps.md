---
author: "Matthieu Joly"
title: "Mon petit printemps"
date: 2023-05-21
description: "Présentation de mon petit printemps"
categories: ["Saison"]
thumbnail: /images/beer/saison/21-mon-petit-printemps.jpg
credits: "<a href=\"https://fr.freepik.com/photos-gratuite/paysages-automne_1254316.htm#query=automne&position=1&from_view=search&track=sph\">Image de photoangel</a> sur Freepik"
translationKey: "my-little-spring"
tags: ["Bière", "Blanche", "Saison"]
---

<h2>Saison</h2>
<h4>Season (Weißbier)</h4>

Douceur, renouvellement de la nature, voilà ce qui caractérise l’arrivée du printemps. C’est exactement ce que j’ai essayé de retranscrire dans cette bière de saison. Laissez votre gros manteau à l’entrée et profitez des rayons de soleil qui se réchauffent.

**ABV**		4,9% <br>
**SRM**		4 <br>
**IBU**		8,7 <br>