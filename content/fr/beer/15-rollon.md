---
author: "Matthieu Joly"
title: "Rollon"
date: 2023-09-15
description: "Présentation de la Rollon"
categories: ["Vikings"]
thumbnail: /images/beer/viking/15-rollon.jpg
tags: ["Bière", "Blonde", "Vikings"]
---

<h2>Vikings</h2>
<h4>Bière de garde</h4>

De Jarl à duché de Normandie, Rollon s’est imposé à l’Europe en s’implantant en France. Il instaure la paix et la sécurité en Normandie ce qui lui vaut des récits légendaires. Cette bière de garde saura stopper vos intentions belliqueuses.

**ABV**		8,4% <br>
**SRM**		8 <br>
**IBU**		22 <br>