---
author: "Matthieu Joly"
title: "Dax"
date: 2023-10-05
description: "Présentation de la Dax"
categories: ["Friends"]
thumbnail: /images/beer/friend/05-dax.jpg
tags: ["Bière", "Brune", "Friends"]
---

<h2>Friends</h2>
<h4>American Brown Ale</h4>

Un beau brun ténébreux ou un ourson en peluche ? Tout ce que l’on sait c’est que sa présence nous rassure et que sa compagnie est agréable. Une douceur dont ne se lasse pas, une texture qui ne nous laisse pas indifférent.

**ABV**		5,5% <br>
**SRM**		20 <br>
**IBU**		25,5 <br>