---
author: "Matthieu Joly"
title: "Mon petit été"
date: 2023-05-22
description: "Présentation de mon petit été"
categories: ["Saison"]
thumbnail: /images/beer/saison/22-mon-petit-ete.jpg
credits: "Image de <a href=\"https://fr.freepik.com/vecteurs-libre/fond-plat-pour-ete_40322931.htm#query=%C3%A9t%C3%A9&position=10&from_view=search&track=sph\">Freepik</a>"
translationKey: "my-little-summer"
tags: ["Bière", "IPA", "Saison"]
---

<h2>Saison</h2>
<h4>American IPA</h4>

Voilà l’été et ses apéros en terrasses, ses barbecues, ses randonnées en forêt et ses soirées au camping. Oui, mais pas sans ma bière, cette bière de saison se partage, comme tous ces petits moments précieux avec la famille et les amis.

**ABV**		6,6% <br>
**SRM**		10 <br>
**IBU**		52,1 <br>