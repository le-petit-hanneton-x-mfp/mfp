---
author: "Matthieu Joly"
title: "Mon petit automne"
date: 2023-05-23
description: "Présentation de mon petit automne"
categories: ["Saison"]
thumbnail: /images/beer/saison/23-mon-petit-automne.jpg
credits: "<a href=\"https://fr.freepik.com/photos-gratuite/paysages-automne_1254316.htm#query=automne&position=1&from_view=search&track=sph\">Image de photoangel</a> sur Freepik"
translationKey: "my-little-autumn"
tags: ["Bière", "Brune", "Saison"]
---

<h2>Saison</h2>
<h4>American Amber Ale</h4>

Une nouvelle saison de nouvelles saveurs. Bien que la température redescende et que la météo se dégrade, il est temps de reprendre les soirées jeux au coin du feu avec cette bière de saison. 

**ABV**		4,7% <br>
**SRM**		19 <br>
**IBU**		34,3 <br>