---
author: "Matthieu Joly"
title: "Mon petit Noël"
date: 2023-05-25
description: "Présentation de Mon petit Noël"
categories: ["Saison"]
thumbnail: /images/beer/saison/25-mon-petit-noel.jpg
credits: "Image de <a href=\"https://fr.freepik.com/vecteurs-libre/paysage-hiver-au-design-plat_10832951.htm#query=hivers&position=8&from_view=search&track=sph\">Freepik</a>"
translationKey: "my-little-winter"
tags: ["Bière", "Winter", "Brune", "Saison"]
---

<h2>Saison</h2>
<h4>Winter Seasonal Beer</h4>

Ma première bière de saison. Plongez dans l’univers des fêtes de fin d’année et enivrez vous des épices délicates qu’offre son bouquet aromatique. De quoi se réchauffer par ces températures glaciale, sa robe sombre cache bien son jeu. Joyeux Noël, restez prudent.

**ABV**		9,4% <br>
**SRM**		41 <br>
**IBU**		9,5 <br>