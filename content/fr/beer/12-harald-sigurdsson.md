---
author: "Matthieu Joly"
title: "Harald Sigurdsson"
date: 2023-09-12
description: "Présentation de la Harald Sigurdsson"
categories: ["Vikings"]
thumbnail: /images/beer/viking/12-harald-sigurdsson.jpg
tags: ["Bière", "Blonde", "Vikings"]
---

<h2>Vikings</h2>
<h4>Belgian Golden Strong Ale</h4>

L’impitoyable Harald Sigurdsson s’illustre de son renforcement de l’autorité de son royaume et ses faits d’armes. Après avoir conquis le Danemark il envahit le Yorkshire en Angleterre. Surnommer le dernier des Vikings, oseriez-vous tentez un bras de faire contre lui ?

**ABV**		11% <br>
**SRM**		5 <br>
**IBU**		25,7 <br>