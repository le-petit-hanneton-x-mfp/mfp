---
author: "Matthieu Joly"
title: "Eirikr Thorvaldson"
date: 2023-09-11
description: "Présentation de la Eirikr Thorvaldson"
categories: ["Vikings"]
thumbnail: /images/beer/viking/11-eirikr-thorvaldson.jpg
tags: ["Bière", "Brune", "Vikings"]
---

<h2>Vikings</h2>
<h4>Irish Red Ale</h4>

Surnommé Eric le rouge, à cause de la couleur de sa barbe et de ses cheveux. Banni d’islande, Eirikr marche ainsi dans les traces de son père banni de Norvège tous deux pour meurtre. Eirikr reste célèbre pour être le fondateur des premières colonies au Groenland. Cette rousse sanguinaire vous fera découvrir des terres nouvelles.

**ABV**		4,3% <br>
**SRM**		12 <br>
**IBU**		18,2 <br>