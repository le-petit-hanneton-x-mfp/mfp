---
author: "Matthieu Joly"
title: "La même"
date: 2023-06-12
description: "Présentation de La même"
categories: ["Autre"]
thumbnail: /images/beer/autre/42-la-meme.jpg
tags: ["Bière", "IPA", "Autre"]
---

<h2>Autre</h2>
<h4>Brown IPA</h4>

Littéralement créée pour tromper le consommateur !
Heu... éveiller la curiosité du consommateur !
Vous voulez rester indécis ou commencer à découvrir des bières brassées pour vous. Pour vous faire voyager, rêver ou...
Vous auriez dû commander la même chose que votre pote.


**ABV**		6,6% <br>
**SRM**		18 <br>
**IBU**		48,7 <br>