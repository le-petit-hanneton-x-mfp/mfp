---
author: "Matthieu Joly"
title: "Tokyo"
date: 2023-06-01
description: "Présentation de la Tokyo"
categories: ["World wide IPA"]
thumbnail: /images/beer/wwipa/31-tokyo.jpg
credits: "<a href=\"https://fr.freepik.com/vecteurs-libre/illustration-paysage-du-japon_7439515.htm#query=Japon&position=0&from_view=search&track=sph\">Image de macrovector</a> sur Freepik"
tags: ["Bière", "IPA", "World wide IPA"]
---

<h2>World wide IPA</h2>
<h4>Rye IPA</h4>

Partons en destination du soleil levant et arrêtons-nous dans le quartier de Chûô-ku. Nous voilà détalant dans le plus grand marché de poisson au monde. Mais arrêtons-nous devant ce petit étal rempli de fruits. L’odeur du citron vert prédomine, lorsqu’un vendeur nous propose un sorbet citron. Vous laisseriez-vous tenter ?

**ABV**		6,5% <br>
**SRM**		9 <br>
**IBU**		36,2 <br>