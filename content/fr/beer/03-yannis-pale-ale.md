---
author: "Matthieu Joly"
title: "Yannis Pale Ale"
date: 2023-10-03
description: "Présentation de la Yannis Pale Ale"
categories: ["Friends"]
thumbnail: /images/beer/friend/03-yannis-pale-ale.jpg
tags: ["Bière", "Blonde", "Friends"]
---

<h2>Friends</h2>
<h4>Swiss Pale Ale</h4>

Associations aléatoires mais contrôlées, la première Swiss Pale Ale nous cause des problèmes différents à chaque brassin ce qui fait sa singularité. Désaltérante et remplie de fraîcheur, vous risquez d’oublier d'où vous venez.

**ABV**		4,7% <br>
**SRM**		8 <br>
**IBU**		25,8 <br>