---
author: "Matthieu Joly"
title: "Emily"
date: 2023-10-07
description: "Présentation de la Emily"
categories: ["Friends"]
thumbnail: /images/beer/friend/07-emily.jpg
tags: ["Bière", "Blanche", "Friends"]
---

<h2>Friends</h2>
<h4>Weißbier</h4>

Voici la délicatesse et la légèreté que vous attendiez d’une bière de terrasse. Son parfum floral envoûtant et son caractère épicée, cette bière blanche aromatisée au thé va vous faire voyager.

**ABV**		4,9% <br>
**SRM**		3 <br>
**IBU**		8,3 <br>
