---
author: "Matthieu Joly"
title: "Popov"
date: 2023-06-11
description: "Présentation de la Popov"
categories: ["Autre"]
thumbnail: /images/beer/autre/41-popov.jpg
tags: ["Bière", "Noire", "Autre"]
---

<h2>Autre</h2>
<h4>Imperial Stout</h4>

Un long périple entre Moscou et St-Pétersbourg vous attend. Ça vous tombe comme un coup de massue sur la tête et vous finissez par parler Russe. 
Za vashe zdorovie !

**ABV**		10% <br>
**SRM**		43 <br>
**IBU**		17,9 <br>