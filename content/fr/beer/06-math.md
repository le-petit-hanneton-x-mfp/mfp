---
author: "Matthieu Joly"
title: "Math"
date: 2023-10-06
description: "Présentation de la Math"
categories: ["Friends"]
thumbnail: /images/beer/friend/06-math.jpg
tags: ["Bière", "IPA", "Friends"]
---

<h2>Friends</h2>
<h4>Belgian IPA</h4>

Méfiez-vous de sa douceur et de sa délicatesse. Son aspect solaire vous emportera dans un jardin jonché de jasmin. Cependant, son amertume légère cache un caractère passionné.

**ABV**		6,2% <br>
**SRM**		11 <br>
**IBU**		48 <br>