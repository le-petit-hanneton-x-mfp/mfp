---
author: "Matthieu Joly"
title: "Ljubjana"
date: 2023-06-04
description: "Présentation de la Ljubjana"
categories: ["World wide IPA"]
thumbnail: /images/beer/wwipa/34-ljubjana.jpg
credits: "<a href=\"https://fr.freepik.com/photos-gratuite/paysage-montagne-nuages-du-brouillard-au-premier-plan_42129598.htm#query=Slov%C3%A9nie&position=3&from_view=search&track=sph\">Image de vecstock</a> sur Freepik"
tags: ["Bière", "IPA", "World wide IPA"]
---

<h2>World wide IPA</h2>
<h4>White IPA</h4>

En redescendant du château de Ljubjana arrêtons-nous un instant au central market. Fruits et légumes colorent les étals, mais arrêtons-nous devant ces épices. Les couleurs en sont tout aussi sublimes, mais les odeurs en sont tout autre. 

**ABV**		5,9% <br>
**SRM**		5 <br>
**IBU**		46,3 <br>