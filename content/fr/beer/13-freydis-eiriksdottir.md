---
author: "Matthieu Joly"
title: "Freydis Eiriksdottir"
date: 2023-09-13
description: "Présentation de la Freydis Eiriksdottir"
categories: ["Vikings"]
thumbnail: /images/beer/viking/13-freydis-eiriksdottir.jpg
tags: ["Bière", "Blonde", "Vikings"]
---

<h2>Vikings</h2>
<h4>Belgian Tripel</h4>

Plongez dans la légende de Freijdis Eirikrsdottir, la fille d’Eirikr Thorvaldson. Femme de caractère, on raconte que lors d’une expédition au Canada, elle aurait tué une dizaine d’islandais car ils l’auraient agacée. C’est pourquoi cette blonde est forte, puissante et risque de vous faire trébucher. Son délicat parfum saura-t-il vous envoûter ?

**ABV**		9,7% <br>
**SRM**		4 <br>
**IBU**		20,6 <br>