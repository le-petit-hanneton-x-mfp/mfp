---
author: "Matthieu Joly"
title: "Egill Skallagrimsson"
date: 2023-09-14
description: "Présentation de la Egill Skallagrimsson"
categories: ["Vikings"]
thumbnail: /images/beer/viking/14-egill-skallagrimsson.jpg
tags: ["Bière", "Noire", "Vikings"]
---

<h2>Vikings</h2>
<h4>Oatmeal Stout</h4>

Un ami loyal, un père dévoué, un brin têtu, revanchard et avide de richesses, voilà qui définit l’un des plus grands scaldes de son époque. Ce poète se laissant mourir à la mort de deux de ces fils reprend le dessus quand sa fille lui force à écrire l’un de ses plus beaux poème “Sonatorrek” où il mélange haine et amour envers Odin, comme il sait si bien le faire.

**ABV**		5,8% <br>
**SRM**		38 <br>
**IBU**		31,4 <br>