---
author: "Matthieu Joly"
title: "Ludovice"
date: 2023-10-02
description: "Présentation de la Ludovice"
categories: ["Friends"]
thumbnail: /images/beer/friend/02-ludovice.jpg
tags: ["Bière", "Noire", "Friends"]
---

<h2>Friends</h2>
<h4>Chocolate Porter</h4>

La Ludovice (se prononce à l’italienne, on est pas dans GTA Vice City Stories), une porter aux arômes de cacao amer, est en canette. Cette bière noire peu pétillante se dégustera parfaitement avec vos desserts. Température de service conseillée aux alentour de 12°C.

**ABV**		5,3% <br>
**SRM**		29 <br>
**IBU**		23,1 <br>