---
author: "Matthieu Joly"
title: "Les brasseurs"
date: 2023-05-23
description: "Présentation des brasseurs"
tags: ["Brasserie"]
thumbnail: /images/blog/les-brasseurs/Yannis-vs-Matthieu.png
translationKey: "brewers"
---

Nous sommes deux amis passionnés de bières artisanales. Nous nous sommes rencontrés en 2011, lors de nos études en informatique. Après avoir fait le tour des conneries à faire, c'est en 2017 que nous brassons notre première bière ensemble. Dès le départ, nous avons pris la décision de ne pas brasser de recettes faites par d'autres. Nous avons pris le temps de faire les recherches nécessaire à la création de différentes recettes. Nous les testons et les éprouvons dans le temps et jusqu'ici nous avons plutôt de bon retour.

# Matthieu

### Brasseur principal

{{< figure src="/images/blog/les-brasseurs/matthieu-portrait.jpg" alt="Portrait Matthieu" >}}

Je suis l'auteur principal des recettes que vous pourrez retrouver sur ce site. Depuis 2017, j'effectue des recherches, j'expérimente et je brasse dans le but de m'améliorer et de découvrir de nouvelles saveurs. J'ai commencé à brasser en tout grain dès la première heure sur un système 6 litres. Il n'y a pas besoin de grand chose pour commencer, une grande casserole, une passoire et un fermenteur. Mon style de bière préféré reste les russian imperial stout, mais je bois de tout ou presque. J'aime partager mes connaissances et imaginer de nouvelles recettes classiques ou insolites.

# Yannis

### Brasseur

{{< figure src="/images/blog/les-brasseurs/yannis-portrait.jpg" alt="Portrait Yannis" >}}

Mon meilleur assistant, auteur de sa première recette, il assiste à presque tous les brassages. Il participe à la promotion de la brasserie et pousse à la réflexion depuis 2017. Il voit les choses en grand, il a débuté avec mon aide directement en tout grain sur un système de 30 litres. Depuis quelques années, il s'intéresse au Sour, des bières acides généralement fruitées. Il récolte depuis un maximum de levure sauvage pour brasser la meilleure Sour. Il apprécie quand il n'y a plus rien à faire et il aime les choses bien faites.
