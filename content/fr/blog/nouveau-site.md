---
author: "Matthieu Joly"
title: "Nouveau site"
date: 2023-05-08
description: "Lancement du nouveau site, petit tour d'horizon"
credits: "<a href=\"http://www.freepik.com\">Designed by stories / Freepik</a>"
tags: ["Info"]
thumbnail: /images/blog/nouveau-site/construct-website.jpg
translationKey: "new-site"
---

Le nouveau site est là. Nous avons choisi de faire un site en français et en anglais. En effet, de plus en plus de non-francophones s'intéressent aux actualités de MFP et souhaitent en apprendre un peu plus sur la brasserie. Ce site a entièrement été réalisé avec le framework Hugo et le thème blist.

Le site se décompose en plusieurs sections. Il y a tous d'abord les bières, vous y trouverez toutes les bières produites ou en production et leur caractéristique. Il y a ensuite la partie blog, ici nous publierons des articles sur la brasserie ou plus général sur le processus de fabrication ou même sur un élément que nous avons envie de partager. Nous mettrons des tags sur ces articles pour pouvoir retrouver plus facilement les articles portant sur la même thématique. L'option de recherche reste disponnible si vous cherchez quelque chose spécifiquement.

Ce site supporte le dark mode donc sentez-vous libre de switcher à votre convenance. Meilleur article contiendra l'article que nous souhaitons mettre en avant, il est possible qu'il change au cours du temps. Vous trouverez également nos réseaux en bas de page.

Nous souhaitons apporter plus de visibilité sur notre projet à travers ce site. Nous pourrons ainsi vous tenir au courant des prochains événements ou des nouveautés au fur et à mesure.

### Alors resté à l'affût