---
author: Matthieu Joly
title: A propos
date: 2023-05-02
description:
keywords: ["about-us", "about-mfp", "contact", "a-propos"]
type: about
---

Bienvenue chez MFP Brasserie, une brasserie artisanale passionnée par l'art de la bière et la convivialité. Nous sommes une équipe de brasseurs passionnés qui croyons que la bière est plus qu'une simple boisson - c'est un symbole de convivialité, de partage et de gourmandise.

Nous sommes fiers de produire des bières uniques et savoureuses, en utilisant des ingrédients de qualité et en respectant l'environnement. Nous sommes constamment à la recherche de nouvelles saveurs et de nouvelles techniques pour créer des bières qui surprennent et enchantent les papilles.

Nous sommes des passionnés menés par l'initiative Matthieu, un brasseur passionné qui a décidé de créer sa propre brasserie pour partager sa passion avec les autres. Notre nom, MFP Brasserie, est un hommage à notre fondateur et à son engagement envers la qualité et l'innovation.

Chez MFP Brasserie, nous croyons que la bière est une boisson qui doit être partagée. C'est pourquoi nous aimons organiser des événements conviviaux et des dégustations de bières, pour que chacun puisse découvrir et apprécier nos créations. Nous croyons également en la curiosité et en l'ouverture d'esprit, et nous aimons explorer de nouveaux horizons pour trouver des idées et des inspirations.

Nous espérons que vous partagerez notre passion pour la bière et que vous vous joindrez à nous pour célébrer la convivialité et la gourmandise. Merci de visiter notre site et de découvrir nos bières artisanales
