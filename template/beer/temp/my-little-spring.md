---
author: "Matthieu Joly"
title: "Mon petit printemps"
date: 2023-04-17
description: "Présentation de mon petit printemps"
thumbnail: /ete.png
translationKey: "my-little-spring"
categories: ["saison"]
---

<h2>Saison</h2>
<h4>Weißbier</h4>

Sweetness, renewal of nature, this is what characterizes the arrival of spring. That's exactly what I tried to capture in this seasonal beer. Leave your heavy coat at the entrance and enjoy the warming rays of the sun.

**ABV**		4,9%<br>
**SRM**		4<br>
**IBU**		8,7<br>
