---
author: "Matthieu Joly"
title: "Mon petit printemps"
date: 2023-04-17
description: "Présentation de mon petit printemps"
thumbnail: /ete.png
translationKey: "my-little-spring"
categories: ["saison"]
---

<h2>Saison</h2>
<h4>Weißbier</h4>

Douceur, renouvellement de la nature, voilà ce qui caractérise l’arrivée du printemps. C’est exactement ce que j’ai essayé de retranscrire dans cette bière de saison. Laissez votre gros manteau à l’entrée et profitez des rayons de soleil qui se réchauffent.

**ABV**		4,9%<br>
**SRM**		4<br>
**IBU**		8,7<br>
