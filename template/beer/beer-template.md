---
author: "Matthieu Joly"
title: ""
date: 2087-04-17
description: ""
thumbnail: /ete.png
credits: ""
translationKey: ""
categories: [""]
---

<h2>Saison</h2>
<h4>Season (American IPA)</h4>

Voilà l’été et ses apéros en terrasses, ses barbecues, ses randonnées en forêt et ses soirées au camping. Oui, mais pas sans ma bière, cette bière de saison se partage, comme tous ces petits moments précieux avec la famille et les amis.

**ABV**		6,6%<br>
**SRM**		10<br>
**IBU**		52,1<br>
